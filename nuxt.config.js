export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Netti',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Netti' },
      { name: 'format-detection', content: 'telephone=no' },
      { hid: 'twitter:image', name: 'twitter:image', content: '/social.png' },
      { hid: 'twitter:card', name: 'twitter:card', content: 'summary_large_image' },
      { hid: 'twitter:title', name: 'twitter:title', content: 'Netti' },
      { hid: 'twitter:description', name: 'twitter:description', content: 'Netti' },
      { hid: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:title', property: 'og:title', content: 'Netti' },
      { hid: 'og:description', property: 'og:description', content: 'Netti' },
      { hid: 'og:image', property: 'og:image', content: '/social.png' },
      { hid: 'og:site_name', property: 'og:site_name', content: 'Netti' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/sass/main.sass',
    '@/assets/sass/grid.sass'
  ],

  styleResources: {
    sass: [
      '@/assets/sass/variables.sass'
    ]
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    ['@nuxtjs/style-resources'],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    ['nuxt-lazy-load', {
      // These are the default values
      images: true,
      videos: true,
      audios: false,
      iframes: false,
      native: false,
      polyfill: true,
      directiveOnly: false,
      offset: 500,
      defaultImage: '/opacity.png',
      // To remove class set value to false
      loadingClass: 'isLoading',
      loadedClass: 'isLoaded',
      appendClass: 'lazyLoad',

      observerConfig: {
        rootMargin: '300px',
      }
    }]
  ],
  render: {
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7
    }
  },
  serverMiddleware: [
    { path: "/api", handler: "~/api/newsletter.js" },
  ],
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
