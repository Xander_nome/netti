import express from 'express';
import Mailchimp from 'mailchimp-api-v3';

// const API_KEY = '39e2d9e89ca4177c6a46785a39608ff2-us17'
// const AUDIENCE_ID = '6cdb0d76fd'
const API_KEY = '5b9152e2c057857f2b1d9fac8370a15d-us10'
const AUDIENCE_ID = '3373e7944b'
const mailchimp = new Mailchimp(API_KEY);

const app = express();
app.use(express.json());

app.post('/subscribe', async(req, res) => {
  const { email: email_address } = req.body

  try {
    const response = await mailchimp.request({
      method: 'post',
      path: `/lists/${AUDIENCE_ID}/members`,
      body: {
        email_address,
        status: "subscribed"
      }
    })
    res.status(response.statusCode).json(response.status);
  } catch(error) {
    res.status(error.status).send(error)
  }
})

module.exports = app
